cd /home/gitlab/stg
tar -xzf project_artifact.tar.gz
rm -rf project_artifact.tar.gz
cp env .env.stg
rm env
pm2 delete stg
pm2 start ./bin/www --name stg
