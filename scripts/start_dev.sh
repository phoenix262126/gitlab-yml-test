cd /home/gitlab/dev
tar -xzf project_artifact.tar.gz
rm -rf project_artifact.tar.gz
cp env .env.dev
rm env
pm2 delete dev
pm2 start ./bin/www --name dev
