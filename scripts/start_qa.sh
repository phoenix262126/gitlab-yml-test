cd /home/gitlab/qa
tar -xzf project_artifact.tar.gz
rm -rf project_artifact.tar.gz
cp env .env.qa
rm env
pm2 delete qa
pm2 start ./bin/www --name qa
